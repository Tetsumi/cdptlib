/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <locale.h>
#include <wchar.h>
#include "../source/base.h"
#include "test.h"

/* ✓ \u2713
 * ⚠ \u26A0
 */
Void Test_do (CPC_Test tests)
{
	U32 fail    = 0;
	U32 success = 0;
	for(Index i = 0; tests[i].proc; ++i)
	{
		wprintf(L"\tTesting %-25s %ls\n",
		       tests[i].caption,
		       (tests[i].proc() == ERROR_NONE ?
			(++success, L"\x1b[32m✓ OK!\x1b[0m") :
			(++fail,    L"\x1b[31m⚠ FAIL!\x1b[0m")));
	}
	
	wprintf(L"-------------------------------------------------\n"
		L"\tSuccess : %3u\n"
		L"\tFail    : %3u\n",
		success,
		fail);
}

extern C_Test gArray_tests[];
extern C_Test gLinkedlist_tests[];
extern C_Test gArraylist_tests[];
extern C_Test gXorshift1024Star_tests[];
extern C_Test gMT19937_tests[];
extern C_Test gHash_tests[];

struct
{
	CPC_Test tests;
	CPC_Char caption;
} const tests[] = {
	{.tests = gArray_tests,            .caption = "Array"},
	{.tests = gLinkedlist_tests,       .caption = "LinkedList"},
	{.tests = gArraylist_tests,        .caption = "ArrayList"},
	{.tests = gXorshift1024Star_tests, .caption = "Xorshift1024Star"},
	{.tests = gMT19937_tests,          .caption = "MT19937"},
	{.tests = gHash_tests,             .caption = "Hash"},
	{.tests = NULL,                    .caption = "End Mark"}
};

int main (Void)
{
	setlocale(LC_CTYPE, "en_US.UTF-8");
	
        for (Index i = 0; tests[i].tests; ++i)
	{
		wprintf(L"Testing %s\n"
			L"-------------------------------------------------\n"
			, tests[i].caption);
		Test_do(tests[i].tests);
		wprintf(L"\n");
	}			
	wprintf(L"Done!\n");
}
