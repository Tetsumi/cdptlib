#pragma once

#include "../source/base.h"

typedef struct
{
	Error (*proc)();
        CPC_Char caption;
} Test;

DEFINE_TYPEDEFS(Test);

void Test_do (CPC_Test tests);
