/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include "../source/base.h"
#include "../source/random.h"
#include "test.h"

static Error test_initialize (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_Seed64 seeds[] = {
		17131907776045769687LLU,  9120621550721899595LLU,
		 5237368999691878260LLU,  2352804886863130741LLU,
		11490144281350267834LLU,  8774848118140167862LLU,
		 9601317632850681100LLU,  9990345221644363062LLU,
		 8677393074463220817LLU, 16872581466014582670LLU,
		15236227833832580513LLU,  9308868350192874793LLU,
		10206613984027032312LLU,  6132570460443609633LLU,
		17912304323102807858LLU,  5474155165282342420LLU
	};
	for (Index i = 0; i < 16; ++i)
		assert(seeds[i] == xs.seeds[i]);
	return ERROR_NONE;
}

static Error test_nextU64 (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_U64 rands[] = {
		10226243819943468057LLU,  2721006985876137581LLU,
		15369317829283885984LLU,  8920398819410157379LLU,
		13973015510038730907LLU, 13270660739349263821LLU,
		 8500199135103818721LLU, 13257283840574039766LLU,
		14826793326247457518LLU, 13396007184539066966LLU,
		17239051717475716380LLU,  9833546291203485122LLU,
		14705170389512432429LLU, 15297058958131851170LLU,
		 5801006457598682876LLU, 15144225914355908517LLU
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextU64(&xs));
	return ERROR_NONE;
}

static Error test_nextR64 (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_R64 rands[] = {
		0x1.1bd5d52584874p-1, 0x1.2e17ab7f44071p-3,
		0x1.aa9592456d168p-1, 0x1.ef2e7ddb17068p-2,
		0x1.83d43f25f9c3dp-1, 0x1.7055b7434f457p-1,
		0x1.d7db184f3fc88p-2, 0x1.6ff6aad367e25p-1,
		0x1.9b86b3f2f2398p-1, 0x1.73d05b1e56105p-1,
		0x1.de7ad466b974bp-1, 0x1.10ef8c80c3166p-1,
		0x1.9826851da8f67p-1, 0x1.a8942426774ffp-1,
		0x1.4205428d73aa9p-2, 0x1.a45632768ee61p-1,
	};
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextR64(&xs));
	return ERROR_NONE;
}

static Error test_nextR32 (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_R32 rands[] = {
		0x1.1bd5d4p-1, 0x1.2e17a8p-3, 0x1.aa9592p-1, 0x1.ef2e7cp-2,
		0x1.83d43ep-1, 0x1.7055b6p-1, 0x1.d7db18p-2, 0x1.6ff6aap-1,
		0x1.9b86b2p-1, 0x1.73d05ap-1, 0x1.de7ad4p-1, 0x1.10ef8cp-1,
		0x1.982684p-1, 0x1.a89424p-1,  0x1.42054p-2, 0x1.a45632p-1, 
	};
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextR32(&xs));
	return ERROR_NONE;
}

static Error test_nextBoolean (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_Boolean rands[] = {1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1};
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextBoolean(&xs));
	return ERROR_NONE;
}

static Error test_nextS64 (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_S64 rands[] = {
		-8220500253766083559LL,  2721006985876137581LL,
		-3077426244425665632LL,  8920398819410157379LL,
		-4473728563670820709LL, -5176083334360287795LL,
		 8500199135103818721LL, -5189460233135511850LL,
		-3619950747462094098LL, -5050736889170484650LL,
		-1207692356233835236LL, -8613197782506066494LL,
		-3741573684197119187LL, -3149685115577700446LL,
		 5801006457598682876LL, -3302518159353643099LL
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextS64(&xs));
	return ERROR_NONE;
}

static Error test_nextMaxU64 (Void)
{
	Xorshift1024Star xs;
	Random_initialize(&xs, 123456789);
	C_U64 rands[] = {
		 28LL, 790LL, 992LL, 689LL, 453LL, 910LL, 360LL, 883LL, 759LL,
		483LL, 190LL, 561LL, 214LL, 585LL, 438LL, 258LL,
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextMaxU64(&xs, 1000));
	return ERROR_NONE;
}

C_Test gXorshift1024Star_tests[] = {
	{.proc = test_initialize,  .caption = "Initialize"},
	{.proc = test_nextU64,     .caption = "Next U64"},
	{.proc = test_nextR64,     .caption = "Next R64"},
	{.proc = test_nextR32,     .caption = "Next R32"},
	{.proc = test_nextBoolean, .caption = "Next Boolean"},
	{.proc = test_nextS64,     .caption = "Next S64"},
	{.proc = test_nextMaxU64,  .caption = "Next max U64"},
	{.proc = NULL,             .caption = "End mark"}
};
