/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include "../source/base.h"
#include "../source/random.h"
#include "test.h"

static Error test_initialize (Void)
{
	MT19937 mt;
	Random_initialize(&mt, 123456789);
	return ERROR_NONE;
}

static Error test_nextU32 (Void)
{
	MT19937 mt;
	Random_initialize(&mt, 4357);
	C_U32 rands[] = {
		 4293858116LU,  699692587LU, 1213834231LU, 4068197670LU,
		  994957275LU, 2082945813LU, 4112332215LU, 3196767107LU,
		 2319469851LU, 3178073856LU, 3263933760LU, 2828822719LU,
		 1355653262LU, 3454884641LU, 2231974739LU,  724013039LU
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextU32(&mt));
	return ERROR_NONE;
}

static Error test_nextS32 (Void)
{
	MT19937 mt;
	Random_initialize(&mt, 4357);
	C_S32 rands[] = {
		  -1109180,   699692587,  1213834231,  -226769626,   994957275,
		2082945813,  -182635081, -1098200189, -1975497445, -1116893440,
	       -1031033536, -1466144577,  1355653262,  -840082655, -2062992557,
		 724013039,
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextS32(&mt));
	return ERROR_NONE;
}

static Error test_nextR32 (Void)
{
	MT19937 mt;
	Random_initialize(&mt, 4357);
	C_R32 rands[] = { 
		0x1.ffde26p-1, 0x1.4da3b2p-3, 0x1.21669p-2,  0x1.e4f78ap-1,
		0x1.da6ebep-3, 0x1.f09cecp-2, 0x1.ea3a6cp-1, 0x1.7d1598p-1,
		0x1.14809ap-1, 0x1.7adb1ep-1, 0x1.85175ap-1, 0x1.5138d2p-1,
		0x1.433682p-2, 0x1.9bdab6p-1, 0x1.0a1276p-1, 0x1.593c8p-3,
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextR32(&mt));
	return ERROR_NONE;
}

static Error test_nextR64 (Void)
{
	MT19937 mt;
	Random_initialize(&mt, 4357);
	C_R64 rands[] = {
		0x1.ffde268a6d1dap-1, 0x1.21668ff93de29p-2,
		0x1.da6ebe7c273b2p-3, 0x1.ea3a6b6fa2b3p-1,
		0x1.14809a2f5b63dp-1, 0x1.85175aaa271a4p-1,
		0x1.43368266f6ad9p-2, 0x1.0a12768ac9e4p-1,
		0x1.e6f1443237586p-2, 0x1.c5f9b53693a66p-3,
		0x1.f10312ab16a02p-6, 0x1.8d9de9f1976c6p-3,
		0x1.28eccdf97dd3bp-1, 0x1.54c4cb9fe93b4p-1,
		0x1.1f0aaa8baa8d4p-1, 0x1.2fa4638f07419p-2,
	};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextR64(&mt));
	return ERROR_NONE;
}

static Error test_nextBoolean (Void)
{
	MT19937 mt;
	Random_initialize(&mt, 4357);
	C_Boolean rands[] = {1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0};	
	for (Index i = 0; i < 16; ++i)
		assert(rands[i] == Random_nextBoolean(&mt));
	return ERROR_NONE;
}

C_Test gMT19937_tests[] = {
	{.proc = test_initialize,  .caption = "Initialize"},
	{.proc = test_nextU32,     .caption = "Next U32"},
	{.proc = test_nextS32,     .caption = "Next S32"},
	{.proc = test_nextR32,     .caption = "Next R32"},
	{.proc = test_nextR64,     .caption = "Next R64"},
	{.proc = test_nextBoolean, .caption = "Next Boolean"},
	{.proc = NULL,             .caption = "End mark"}
};
