/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include "../source/base.h"
#include "../source/hash.h"
#include "test.h"

static Error test_murmurU64 (Void)
{
	U8 data[160];
	C_U32 seed = 123456789;
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0xd682a8c046e2fd45 == Hash_murmurU64(data,   1, seed));
	assert(0xdc53b3dfff33e2d0 == Hash_murmurU64(data,   5, seed));
	assert(0x95b5fc7dde960494 == Hash_murmurU64(data,   7, seed));
	assert( 0x924df3eeab52176 == Hash_murmurU64(data,   8, seed));
	assert(0xbc56ae7df0a889b8 == Hash_murmurU64(data,  12, seed));
	assert(0xec8791d5a5401eba == Hash_murmurU64(data,  15, seed));
	assert(0x82ea1ad21dc8e27c == Hash_murmurU64(data,  16, seed));
	assert(0xc1c6809c2a327941 == Hash_murmurU64(data,  32, seed));
	assert(0xcea281cea6da9087 == Hash_murmurU64(data, 117, seed));
	assert(0xfa40d7c641b24c60 == Hash_murmurU64(data, 160, seed));		
	return ERROR_NONE;
}

static Error test_murmurU32 (Void)
{
	U8 data[160];
	C_U32 seed = 123456789;
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0xeb79e373 == Hash_murmurU32(data,   1, seed));
	assert(0x32271892 == Hash_murmurU32(data,   5, seed));
	assert(0x84d81df9 == Hash_murmurU32(data,   7, seed));
	assert(0xc1292917 == Hash_murmurU32(data,   8, seed));
	assert(0x9225e64d == Hash_murmurU32(data,  12, seed));
	assert(0xfd479b4e == Hash_murmurU32(data,  15, seed));
	assert(0x1e472cc7 == Hash_murmurU32(data,  16, seed));
	assert(0x41c81e3c == Hash_murmurU32(data,  32, seed));
	assert(0xccc49d37 == Hash_murmurU32(data, 117, seed));
	assert(0xef6075fb == Hash_murmurU32(data, 160, seed));		
	return ERROR_NONE;
}

static Error test_rotatingU32 (Void)
{
	U8 data[160];
	C_U32 seed = 123456789;
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0x75bcd140 == Hash_rotatingU32(data,   1, seed));
	assert(0xd1006788 == Hash_rotatingU32(data,   5, seed));
	assert(0x20678887 == Hash_rotatingU32(data,   7, seed));
	assert(0x678887a  == Hash_rotatingU32(data,   8, seed));
	assert(0x887e8fd3 == Hash_rotatingU32(data,  12, seed));
	assert(0xd8fd3459 == Hash_rotatingU32(data,  15, seed));
	assert(0x8fd3458d == Hash_rotatingU32(data,  16, seed));
	assert(0x75bcd35  == Hash_rotatingU32(data,  32, seed));
	assert(0x5eff9870 == Hash_rotatingU32(data, 117, seed));
	assert(0x75bcdb5  == Hash_rotatingU32(data, 160, seed));		
	return ERROR_NONE;
}

static Error test_djbU32 (Void)
{
	U8 data[160];
	C_U32 seed = 123456789;
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0xf2d56f94 == Hash_djbU32(data,   1, seed));
	assert(0x2f170b94 == Hash_djbU32(data,   5, seed));
	assert(0x2945b975 == Hash_djbU32(data,   7, seed));
	assert(0xc28d711d == Hash_djbU32(data,   8, seed));
	assert(0xebf8d99  == Hash_djbU32(data,  12, seed));
	assert(0x122e8075 == Hash_djbU32(data,  15, seed));
	assert(0x1f0f6905 == Hash_djbU32(data,  16, seed));
	assert(0x2501b535 == Hash_djbU32(data,  32, seed));
	assert(0x6a244954 == Hash_djbU32(data, 117, seed));
	assert(0xa9b35db5 == Hash_djbU32(data, 160, seed));
	return ERROR_NONE;
}

static Error test_saxU32 (Void)
{
	U8 data[160];
	C_U32 seed = 123456789;
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0xea0b58d1 == Hash_saxU32(data,   1, seed));
	assert(0xd3bf2db4 == Hash_saxU32(data,   5, seed));
	assert(0x7d8cb464 == Hash_saxU32(data,   7, seed));
	assert(0xc8aafb08 == Hash_saxU32(data,   8, seed));
	assert(0xe9330f90 == Hash_saxU32(data,  12, seed));
	assert(0x705a362e == Hash_saxU32(data,  15, seed));
	assert(0x69bf5ced == Hash_saxU32(data,  16, seed));
	assert(0xc389fba5 == Hash_saxU32(data,  32, seed));
	assert(0x339bc724 == Hash_saxU32(data, 117, seed));
	assert(0x95d9870c == Hash_saxU32(data, 160, seed));
	return ERROR_NONE;
}

static Error test_fnvU32 (Void)
{
	U8 data[160];
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0x50c5d1f  == Hash_fnvU32(data,   1));
	assert(0x1e2f1007 == Hash_fnvU32(data,   5));
	assert(0xf3fee106 == Hash_fnvU32(data,   7));
	assert(0x203c3c75 == Hash_fnvU32(data,   8));
	assert(0x7d89d01  == Hash_fnvU32(data,  12));
	assert(0xca415ace == Hash_fnvU32(data,  15));
	assert(0x32e1f245 == Hash_fnvU32(data,  16));
	assert(0xc3b79145 == Hash_fnvU32(data,  32));
	assert(0xddaec837 == Hash_fnvU32(data, 117));
	assert(0xd9252a45 == Hash_fnvU32(data, 160));
	return ERROR_NONE;
}

static Error test_fnvU64 (Void)
{
	U8 data[160];
	for (Index i = 0; i < 160; ++i)
		data[i] = i;
	assert(0xaf63bd4c8601b7df == Hash_fnvU64(data,   1));
	assert(0xdc199fd92049af47 == Hash_fnvU64(data,   5));
	assert(0xa235a6fae0c6fee6 == Hash_fnvU64(data,   7));
	assert(0x6829a24bf22320d5 == Hash_fnvU64(data,   8));
	assert(0xf0cbbfcb24ef5661 == Hash_fnvU64(data,  12));
	assert(0x49f912a7993c80ae == Hash_fnvU64(data,  15));
	assert(0xeebb60c961cea7a5 == Hash_fnvU64(data,  16));
	assert(0x2a182d05a1c8ea5  == Hash_fnvU64(data,  32));
	assert(0xacfb3a8cbe1911b7 == Hash_fnvU64(data, 117));
	assert(0x90f1435a3ef954a5 == Hash_fnvU64(data, 160));
	return ERROR_NONE;
}

C_Test gHash_tests[] = {
	{.proc = test_murmurU64,   .caption = "Murmur U64"},
	{.proc = test_murmurU32,   .caption = "Murmur U32"},
	{.proc = test_rotatingU32, .caption = "Rotating U32"},
	{.proc = test_djbU32,      .caption = "Djb U32"},
	{.proc = test_saxU32,      .caption = "Sax U32"},
	{.proc = test_fnvU32,      .caption = "Fnv U32"},
	{.proc = test_fnvU64,      .caption = "Fnv U64"},
	{.proc = NULL,             .caption = "End mark"}
};

