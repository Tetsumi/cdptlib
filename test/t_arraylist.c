/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <assert.h>
#include "../source/base.h"
#include "../source/arraylist.h"
#include "test.h"

typedef struct
{
	int a;
	char b[4];
} Foo;

DEFINE_ARRAYLIST(Foo);

static Error test_initialize (Void)
{
	ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 16);
	assert(ERROR_NONE == e);
	assert(0 == ArrayListFoo_length(&al));
	assert(16 == ArrayListFoo_capacity(&al));
	return ERROR_NONE;
}

static Error test_length (Void)
{
        ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 16);
	assert(ERROR_NONE == e);
	assert(0 == ArrayListFoo_length(&al));
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(1 == ArrayListFoo_length(&al));
	e = ArrayListFoo_removeLast(&al, NULL);
	assert(ERROR_NONE == e);
        assert(0 == ArrayListFoo_length(&al));
	return ERROR_NONE;
}

static Error test_size (Void)
{
        ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	C_Length capacity = 16;
	Error e = ArrayListFoo_initialize(&al, capacity);
	assert(ERROR_NONE == e);
	assert(sizeof(ArrayListFoo) + (sizeof(Foo) * capacity)
	       == ArrayListFoo_size(&al));
	for (Index i = 0; i < 32; ++i)
	{
		e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
		assert(ERROR_NONE == e);
	}
	assert(sizeof(ArrayListFoo) + (sizeof(Foo) * ArrayListFoo_capacity(&al))
	       == ArrayListFoo_size(&al));
	return ERROR_NONE;
}

static Error test_capacity (Void)
{
	ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 0);
	assert(ERROR_NONE == e);
	assert(0 == ArrayListFoo_capacity(&al));
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(2 == ArrayListFoo_capacity(&al));
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(6 == ArrayListFoo_capacity(&al));
	return ERROR_NONE;
}

static Error test_ensureCapacity (Void)
{
        ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 0);
	assert(ERROR_NONE == e);
	assert(0 == ArrayListFoo_capacity(&al));
	e = ArrayListFoo_ensureCapacity(&al, 10);
	assert(10 == ArrayListFoo_capacity(&al));
	e = ArrayListFoo_ensureCapacity(&al, 15);
	assert(15 == ArrayListFoo_capacity(&al));
	return ERROR_NONE;
}

static Error test_trimCapacity (Void)
{
        ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 15);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_trimCapacity(&al);
	assert(0 == ArrayListFoo_capacity(&al));
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_ensureCapacity(&al, 10);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_trimCapacity(&al);
	assert(4 == ArrayListFoo_capacity(&al));
	return ERROR_NONE;
}

static Error test_addLast (Void)
{
	ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 15);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(1234 == ArrayListFoo_last(&al).a);
	assert(1 == ArrayListFoo_length(&al));
	e = ArrayListFoo_addLast(&al, (Foo){.a = 5678, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(5678 == ArrayListFoo_last(&al).a);
	assert(2 == ArrayListFoo_length(&al));
	return ERROR_NONE;
}

static Error test_removeLast (Void)
{
	ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 15);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 5678, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 9012, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	Foo foo;
	e = ArrayListFoo_removeLast(&al, &foo);
	assert(ERROR_NONE == e);
	assert(9012 == foo.a);
	assert(2 == ArrayListFoo_length(&al));
	e = ArrayListFoo_removeLast(&al, &foo);
	assert(ERROR_NONE == e);
	assert(5678 == foo.a);
	assert(1 == ArrayListFoo_length(&al));
	e = ArrayListFoo_removeLast(&al, &foo);
	assert(ERROR_NONE == e);
	assert(1234 == foo.a);
	assert(0 == ArrayListFoo_length(&al));
	return ERROR_NONE;
}

static Error test_last (Void)
{
        ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 15);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 5678, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(5678 == ArrayListFoo_last(&al).a);
	e = ArrayListFoo_removeLast(&al, NULL);
	assert(ERROR_NONE == e);
	assert(1234 == ArrayListFoo_last(&al).a);
	return ERROR_NONE;
}

static Error test_lastPointer (Void)
{
        ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 15);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 5678, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	assert(5678 == ArrayListFoo_lastPointer(&al)->a);
	e = ArrayListFoo_removeLast(&al, NULL);
	assert(ERROR_NONE == e);
	assert(1234 == ArrayListFoo_lastPointer(&al)->a);
	return ERROR_NONE;
}

static Error test_clear (Void)
{
	ArrayListFoo al CLEANUP(ArrayListFoo_finalizeCleanup) = ArrayList_STATIC;
	Error e = ArrayListFoo_initialize(&al, 15);
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 1234, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_addLast(&al, (Foo){.a = 5678, .b = {1,2,3,4}});
	assert(ERROR_NONE == e);
	e = ArrayListFoo_clear(&al);
	assert(ERROR_NONE == e);
	assert(0 == ArrayListFoo_length(&al));
	assert(0 == ArrayListFoo_capacity(&al));
	return ERROR_NONE;
}

C_Test gArraylist_tests[] = {
	{.proc = test_initialize,     .caption = "Initialize"},
	{.proc = test_size,           .caption = "Size"},
	{.proc = test_length,         .caption = "Length"},
	{.proc = test_capacity,       .caption = "Capacity"},
	{.proc = test_ensureCapacity, .caption = "Ensure capacity"},
	{.proc = test_trimCapacity,   .caption = "Trim capacity"},
	{.proc = test_addLast,        .caption = "Add last"},
	{.proc = test_removeLast,     .caption = "Remove Last"},
	{.proc = test_last,           .caption = "Last"},
	{.proc = test_lastPointer,    .caption = "Last pointer"},
	{.proc = test_clear,          .caption = "Clear"},
	{.proc = NULL,                .caption = "End mark"}
};
