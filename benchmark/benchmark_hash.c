/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <assert.h>
#include "../source/base.h"
#include "../source/hash.h"
#include "../source/random.h"
#include "benchmark.h"

static Time benchmark_murmurU64 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash64 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_murmurU64(data, length, 123456789);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

static Time benchmark_murmurU32 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash32 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_murmurU32(data, length, 123456789);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

static Time benchmark_rotatingU32 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash32 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_rotatingU32(data, length, 123456789);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

static Time benchmark_djbU32 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash32 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_djbU32(data, length, 123456789);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

static Time benchmark_saxU32 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash32 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_saxU32(data, length, 123456789);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

static Time benchmark_fnvU32 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash32 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_fnvU32(data, length);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

static Time benchmark_fnvU64 (C_Length length, C_Length loop)
{
	Xorshift1024Star xs;
	Xorshift1024Star_initialize(&xs, 593750461);
	U8 data[length];
	for (Index i = 0; i < length; ++i)
		data[i] = Random_nextU64(&xs);
	C_Time start = Benchmark_time();
	Hash64 h = 0;
	for (Index i = 0; i < loop; ++i)
		h += Hash_fnvU64(data, length);
	C_Time elapsed = Benchmark_time() - start;
        printf("", h);
	return elapsed;
}

typedef struct
{	
	Time (*proc)(C_Length length, C_Length loop);
	CPC_Char caption;	
} Benchmark;

DEFINE_TYPEDEFS(Benchmark);

static C_Benchmark gBenchmarks[] = {
	{.proc = benchmark_murmurU64,   .caption = "murmur64"},
	{.proc = benchmark_murmurU32,   .caption = "murmur32"},
	{.proc = benchmark_rotatingU32, .caption = "rotating32"},
	{.proc = benchmark_djbU32,      .caption = "djb32"},
	{.proc = benchmark_saxU32,      .caption = "sax32"},
	{.proc = benchmark_fnvU32,      .caption = "fnv32"},
	{.proc = benchmark_fnvU64,      .caption = "fnv64"},
	{.proc = NULL,                  .caption = "End mark"}
};

int main (int argc, char *argv[])
{
	C_Length length = 1 < argc ? strtoul(argv[1], NULL, 0) : 1024 * 2;
	C_Length loop   = 2 < argc ? strtoul(argv[2], NULL, 0) : 10000000;
	puts("Hash Benchmark:\n"
	     "===============\n");
	for(Index i = 0; gBenchmarks[i].proc; ++i)
	{		
		C_Time t = gBenchmarks[i].proc(length, loop);
		printf("%s\n"
		       "\tLength: %14llu bytes\n"
		       "\ttime  : %14.2f ms\n"
		       "\tflow  : %14.2f mb\\s\n",
		       gBenchmarks[i].caption,
		       length * loop,
			       t / 1000000.0,
		       ((length * loop) / 1048576.0) / (t / 1000000000.0));
	}
}
