cdptlib
===================

General utility library for GNU C or, more precisely, a place to experiment with C hackery.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

#### TODO/Suggestions:
* Documentation
* Benchmarks
* Unit testing 
* Hashing procedures
* Hash table type
* Map table type
* Rand48 type
* Sorting procedures
* Concurrent data structures
* Memory allocators
* String type
* Queue type
* Dequeue type
* Stack type
* CSV
* Cryptography
* DEC64 type
* Stream type
* Timer type
* Binary search
* Logging
* Arguments line parsing
* Base64

