/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#pragma once

#include <assert.h>
#include <string.h>
#include "base.h"

#define DEFINE_ARRAY_INITIALIZE(Type)					\
	static inline Error Array##Type##_initialize			\
	(CPP_Array##Type dest, C_Length length)				\
	{								\
		assert(dest);						\
		*dest = malloc(sizeof(Array##Type)			\
			       + (sizeof(Type) * length));		\
		if (!*dest)						\
			return ERROR_MEMORY;				\
		*(Length*)&(*dest)->length = length;			\
		return ERROR_NONE;					\
	};

#define DEFINE_ARRAY_LENGTH(Type)\
	static inline Length Array##Type##_length(CPC_Array##Type a)  \
	{							      \
		assert(a);					      \
		return a->length;				      \
	}

#define DEFINE_ARRAY_FINALIZE(Type) \
	static inline Void Array##Type##_finalize(P_Array##Type a)	\
	{								\
		free(a);						\
	}

#define DEFINE_ARRAY_FINALIZE_CLEANUP(Type) \
	static inline Void Array##Type##_finalizeCleanup(PP_Array##Type a) \
	{								\
		free(*a);						\
	}

#define DEFINE_ARRAY_GET(Type) \
	static inline Type Array##Type##_get(CPC_Array##Type a, C_Index i) \
	{								\
		assert(a);						\
		assert(Array##Type##_length(a) > i);			\
		return a->elements[i];					\
	}

#define DEFINE_ARRAY_GET_POINTER(Type)					\
	static inline Type *Array##Type##_getPointer			\
	(CP_Array##Type a, C_Index i)					\
	{								\
		assert(a);						\
		assert(Array##Type##_length(a) > i);			\
		return &(a->elements[i]);				\
	}

#define DEFINE_ARRAY_SET(Type) \
	static inline Void Array##Type##_set				\
	(CP_Array##Type a, C_Index i, Type e)				\
	{								\
		assert(a);						\
		assert(Array##Type##_length(a) > i);			\
	        a->elements[i] = e;					\
	}

#define DEFINE_ARRAY_SIZE(Type) \
	static inline Size Array##Type##_size (CPC_Array##Type a)	\
	{								\
		assert(a);						\
		return sizeof(Array##Type) +				\
			(Array##Type##_length(a) * sizeof(Type));	\
	}

#define DEFINE_ARRAY_COPY(Type)						\
	static inline Void Array##Type##_copy				\
	(CP_Array##Type dest, CPC_Array##Type src)			\
	{								\
		assert(dest);						\
		assert(src);						\
		assert(dest != src);					\
		assert(Array##Type##_length(dest) >=			\
		       Array##Type##_length(src));			\
		memcpy(&dest->elements,					\
		       &src->elements,					\
		       Array##Type##_length(src) * sizeof(Type));	\
	}

#define DEFINE_ARRAY_CLONE(Type)					\
	static inline Error Array##Type##_clone				\
	(PP_Array##Type dest, CPC_Array##Type src)			\
	{								\
		assert(src);						\
		Error e = Array##Type##_initialize(dest,		\
						   Array##Type##_length(src)); \
		if (ERROR_NONE != e)					\
			return e;					\
		Array##Type##_copy(*dest, src);				\
		return ERROR_NONE;					\
	}
						
#define DEFINE_ARRAY(Type)			\
	typedef struct Array##Type {		\
		C_Length length;		\
		Type elements[0];		\
	} Array##Type;				\
	DEFINE_TYPEDEFS(Array##Type);		\
	DEFINE_ARRAY_INITIALIZE(Type);		\
	DEFINE_ARRAY_LENGTH(Type);		\
	DEFINE_ARRAY_FINALIZE(Type);		\
	DEFINE_ARRAY_FINALIZE_CLEANUP(Type);	\
	DEFINE_ARRAY_GET(Type);			\
	DEFINE_ARRAY_GET_POINTER(Type);		\
	DEFINE_ARRAY_SET(Type);			\
	DEFINE_ARRAY_SIZE(Type);		\
	DEFINE_ARRAY_COPY(Type);		\
	DEFINE_ARRAY_CLONE(Type);		
/*
// Unused (unsafe)  
#define Array_length(array) ((array)->length)
#define Array_finalize free
#define Array_element(array, nth) ((array)->elements[(nth)])
*/

DEFINE_ARRAY(U8);
DEFINE_ARRAY(U16);
DEFINE_ARRAY(U32);
DEFINE_ARRAY(U64);
DEFINE_ARRAY(S8);
DEFINE_ARRAY(S16);
DEFINE_ARRAY(S32);
DEFINE_ARRAY(S64);
DEFINE_ARRAY(R32);
DEFINE_ARRAY(R64);
DEFINE_ARRAY(Size);
DEFINE_ARRAY(Char);
DEFINE_ARRAY(Boolean);
DEFINE_ARRAY(P_Void);
DEFINE_ARRAY(P_U8);
DEFINE_ARRAY(P_U16);
DEFINE_ARRAY(P_U32);
DEFINE_ARRAY(P_U64);
DEFINE_ARRAY(P_S8);
DEFINE_ARRAY(P_S16);
DEFINE_ARRAY(P_S32);
DEFINE_ARRAY(P_S64);
DEFINE_ARRAY(P_R32);
DEFINE_ARRAY(P_R64);
DEFINE_ARRAY(P_Size);
DEFINE_ARRAY(P_Char);
DEFINE_ARRAY(P_Boolean);
