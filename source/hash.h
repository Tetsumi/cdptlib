/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include <assert.h>
#include "base.h"

static inline Hash64 Hash_murmurU64 (CPC_Void data, C_Length length, C_U32 seed)
{
	assert(data);
	assert(length);
        C_U64     m = 0xc6a4a7935bd1e995;
        C_U32     r = 47;
        Hash64    h = seed ^ length;
        PC_U64 dU64 = (PC_U64)data;
        CPC_U64  end = dU64 + (length >> 3);	
        while(dU64 != end)
        {
                U64 k = *(dU64++);
                k *= m;
                k ^= k >> r;
                k *= m;
                h ^= k;
                h *= m;
        }	
        CPC_U8 dU8 = (CPC_U8)dU64;
        switch(length & 7)
        {
	case 7: h ^= ((uint64_t)dU8[6]) << 48;
	case 6: h ^= ((uint64_t)dU8[5]) << 40;
	case 5: h ^= ((uint64_t)dU8[4]) << 32;
	case 4: h ^= ((uint64_t)dU8[3]) << 24;
	case 3: h ^= ((uint64_t)dU8[2]) << 16;
	case 2: h ^= ((uint64_t)dU8[1]) << 8;
	case 1: h ^= ((uint64_t)dU8[0]);
		h *= m;
        };
        h ^= h >> r;
        h *= m;
        return h ^ (h >> r);
}

static inline Hash32 Hash_murmurU32 (CPC_Void data, C_Length length, C_U32 seed)
{
	assert(data);
	assert(length);
#define ROT32(x, y) (x << y) | (x >> (32 - y))	
	C_U32 c1 = 0xcc9e2d51;
	C_U32 c2 = 0x1b873593;
	C_U32 r1 = 15;
	C_U32 r2 = 13;
	C_U32 m  = 5;
        C_U32 n  = 0xe6546b64;
	Hash32  hash    = seed;
	C_U32   nblocks = length / 4;
	CPC_U32 blocks  = (CPC_U32)data;
	Index i;
	U32 k;
	for (i = 0; i < nblocks; i++)
	{
		k = blocks[i];
		k *= c1;
		k = ROT32(k, r1);
		k *= c2;
		hash ^= k;
		hash = ROT32(hash, r2) * m + n;
	}
	CPC_U8 tail = (CPC_U8)(data + nblocks * 4);
        U32 k1 = 0;
	switch (length & 3)
	{
	case 3:
		k1 ^= tail[2] << 16;
	case 2:
		k1 ^= tail[1] << 8;
	case 1:
		k1 ^= tail[0];

		k1 *= c1;
		k1 = ROT32(k1, r1);
		k1 *= c2;
		hash ^= k1;
	}
	hash ^= length;
	hash ^= (hash >> 16);
	hash *= 0x85ebca6b;
	hash ^= (hash >> 13);
	hash *= 0xc2b2ae35;
	return hash ^ (hash >> 16);
#undef ROT32
}

static inline Hash32 Hash_rotatingU32 (CPC_Void data, C_Length length, C_U32 seed)
{
	assert(data);
	assert(length);
        CPC_U8 bytes = (CPC_U8)data;
	Hash32 hash  = seed ^ length;
	for (Index i = 0; i < length; ++i)
		hash = (hash << 4) ^ (hash >> 28) ^ bytes[i];
	return hash;
}

static inline Hash32 Hash_djbU32 (CPC_Void data, C_Length length, C_U32 seed)
{
	assert(data);
	assert(length);
	CPC_U8 bytes = (CPC_U8)data;
	Hash32 hash = seed ^ length;
	for (Index i = 0; i < length; ++i)
		hash = 33 * hash ^ bytes[i];
	return hash;
}

static inline Hash32 Hash_saxU32 (CPC_Void data, C_Length length, C_U32 seed)
{
	assert(data);
	assert(length);
	CPC_U8 bytes = (CPC_U8)data;
	Hash32 hash = seed ^ length;
	for (Index i = 0; i < length; ++i)
		hash ^= (hash << 5) + (hash >> 2) + bytes[i];
	return hash;
}


static inline Hash32 Hash_fnvU32 (CPC_Void data, C_Length length)
{
	assert(data);
	assert(length);
	CPC_U8 bytes = (CPC_U8)data;
	Hash32 hash = 2166136261;
	for (Index i = 0; i < length; ++i)
		hash = (hash * 16777619) ^ bytes[i];
	return hash;
}
 
static inline Hash64 Hash_fnvU64 (CPC_Void data, C_Length length)
{
	assert(data);
	assert(length);
	CPC_U8 bytes = (CPC_U8)data;
	Hash64 hash = 14695981039346656037LLU;
	for (Index i = 0; i < length; ++i)
		hash = (hash * 1099511628211LLU) ^ bytes[i];
	return hash;
}
