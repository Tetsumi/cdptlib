/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../source/base.h"
#include "../source/array.h"

typedef struct
{
	int  a;
	char b[256];
} Foo;

// Defines the typedefs for our type.
DEFINE_TYPEDEFS(Foo);

// Defines the array type for our type.
DEFINE_ARRAY(Foo);

Void printArray (CP_ArrayFoo a)
{
	assert(a);
	for (Index i = 0; i < ArrayFoo_length(a); ++i)
	{
		// Retrieves a pointer to the nth element
		P_Foo element = ArrayFoo_getPointer(a, i);
		
		printf("Foo(%4d, %8s)\n", element->a, element->b);
	}
}

Void fillArray (CP_ArrayFoo a)
{
	assert(a);
	for (Index i = 0; i < ArrayFoo_length(a); ++i)
	{
		P_Foo element = ArrayFoo_getPointer(a, i);
		element->a = i;
		sprintf(element->b, "%f", 1.0 / i);
	}
}


int main (int argc, char *argv[])
{
	Length length = 10;
	
	if (2 == argc)
		length = atoi(argv[1]);

	// CLEANUP automatically frees our array when leaving the scope.	
	P_ArrayFoo a CLEANUP(ArrayFoo_finalizeCleanup);

	// Initialize our array
	Error e = ArrayFoo_initialize(&a, length);

	// We check for an error.
	if (ERROR_NONE != e)
	{
		fprintf(stderr,
			"ERROR when initializing the array: %u\n",
			e);
		return EXIT_FAILURE;
	}

	fillArray(a);
	printArray(a);
}
