/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "../source/base.h"
#include "../source/linkedlist.h"

typedef struct
{
	int a;
	float b;
	char c[4];
} Foo;

// Defines the typedefs for our type.
DEFINE_TYPEDEFS(Foo);

// Defines the linked list type for our type.
DEFINE_LINKEDLIST(Foo);

int main (int argc, char *argv[])
{
	Length length = 10;
	
	if (2 == argc)
		length = atoi(argv[1]);
	
	// CLEANUP automatically frees our list when leaving the scope.
	P_LinkedListFoo listFoo CLEANUP(LinkedListFoo_finalizeCleanup);
	
	// Initializes our linkedlist.
	Error e = LinkedListFoo_initialize(&listFoo);

	// We check for an error.
	if (ERROR_NONE != e)
	{
		fprintf(stderr,
			"ERROR when initializing the linkedlist: %u\n",
			e);
		return EXIT_FAILURE;
	}
	
	for (Index i = 0; i < length; ++i)
	{
		C_Char c = ((i * 4) % 74) + 48;
		
		//Adds an element as new last element.
		Error e = LinkedListFoo_addLast(listFoo,
					       (Foo){
						   .a = i,
						   .b = i * 0.5,
					           .c = {c, c + 1, c + 2, c + 3}
					       });
		// We check for an error.
		if (ERROR_NONE != e)
		{
			fprintf(stderr,
				"ERROR when adding a new element: %u\n",
				e);
			return EXIT_FAILURE;
		}
	}

	// Defines an iterator for our linked list.
	Iterator iter = LinkedListFoo_iterator(listFoo);

	// Iterates while the iterator has elements.
	while(Iterator_hasNext(iter))
	{
		// Retrieves the next element.
		Foo foo = Iterator_next(iter);
		
	        printf("Foo(a = %2d, b = %5.2f, c = { %c %c %c %c })\n",
		       foo.a,
		       foo.b,
		       foo.c[0],
		       foo.c[1],
		       foo.c[2],
		       foo.c[3]);
	}
}
