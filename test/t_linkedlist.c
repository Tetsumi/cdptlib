/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <assert.h>
#include "../source/base.h"
#include "../source/linkedlist.h"
#include "test.h"

typedef struct
{
	int a;
	char b[4];
} Foo;

DEFINE_LINKEDLIST(Foo);

static Error test_initialize (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	return ERROR_NONE;
}

static Error test_length (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	assert(0 == LinkedListFoo_length(llfoo));
	for (Index i = 0; i < 11; ++i)
	{
		e = LinkedListFoo_addFirst(llfoo,
					   (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
	        assert(i + 1 == LinkedListFoo_length(llfoo));
	}
	for (Index i = 11; i > 0; --i)
	{
		LinkedListFoo_removeFirst(llfoo);
		assert(i - 1 == LinkedListFoo_length(llfoo));
	}
	assert(0 == LinkedListFoo_length(llfoo));
	return ERROR_NONE;
}

static Error test_addFirst (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	for (Index i = 0; i < 10; ++i)
	{
		e = LinkedListFoo_addFirst(llfoo,
					   (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
		assert(LinkedListFoo_first(llfoo).a == i);
		assert(i + 1 == LinkedListFoo_length(llfoo));
	}
	return ERROR_NONE;
}

static Error test_addLast (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	for (Index i = 0; i < 10; ++i)
	{
		e = LinkedListFoo_addLast(llfoo,
					  (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
		assert(LinkedListFoo_last(llfoo).a == i);
		assert(i + 1 == LinkedListFoo_length(llfoo));
	}
	return ERROR_NONE;
}

static Error test_add (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	for (Index i = 0; i < 10; ++i)
	{
		e = LinkedListFoo_add(llfoo, i,
				      (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
		assert(LinkedListFoo_at(llfoo, i).a == i);
		assert(i + 2 == LinkedListFoo_length(llfoo));
	}
	return ERROR_NONE;
}

static Error test_removeFirst (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	LinkedListFoo_addFirst(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	LinkedListFoo_addFirst(llfoo, (Foo){.a = 5678, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo foo = LinkedListFoo_removeFirst(llfoo);
	assert(1 == LinkedListFoo_length(llfoo));
	assert(5678 == foo.a);
	foo = LinkedListFoo_removeFirst(llfoo);
	assert(0 == LinkedListFoo_length(llfoo));
	assert(1234 == foo.a);
	return ERROR_NONE;
}

static Error test_removeLast (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 5678, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo foo = LinkedListFoo_removeLast(llfoo);
	assert(1 == LinkedListFoo_length(llfoo));
	assert(1234 == foo.a);
	foo = LinkedListFoo_removeLast(llfoo);
	assert(0 == LinkedListFoo_length(llfoo));
	assert(5678 == foo.a);
	return ERROR_NONE;
}

static Error test_remove (Void)
{
	 P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 5678, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 9012, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo foo = LinkedListFoo_remove(llfoo, 1);
	assert(2 == LinkedListFoo_length(llfoo));
	assert(5678 == foo.a);
	foo = LinkedListFoo_remove(llfoo, 0);
	assert(1 == LinkedListFoo_length(llfoo));
	assert(9012 == foo.a);
	foo = LinkedListFoo_remove(llfoo, 0);
	assert(0 == LinkedListFoo_length(llfoo));
	assert(1234 == foo.a);
	return ERROR_NONE;
}

static Error test_iterator (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	for (Index i = 0; i < 100; ++i)
	{
		e = LinkedListFoo_addLast(llfoo,
					  (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
	}
	Iterator iter = LinkedListFoo_iterator(llfoo);
	Index i = 0;
	while(Iterator_hasNext(iter))
	{
		Foo foo = Iterator_next(iter);
		assert(foo.a == i++);
	}
	return ERROR_NONE;
}

static Error test_clear (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	for (Index i = 0; i < 100; ++i)
	{
		e = LinkedListFoo_addLast(llfoo,
					  (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
	}
	LinkedListFoo_clear(llfoo);
	assert(0 == LinkedListFoo_length(llfoo));
	return ERROR_NONE;
}

static Error test_firstPointer (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 8456, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo *p = LinkedListFoo_firstPointer(llfoo);
	assert(1234 == p->a);
	return ERROR_NONE;
}

static Error test_lastPointer (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 8456, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo *p = LinkedListFoo_lastPointer(llfoo);
	assert(1234 == p->a);
	return ERROR_NONE;
}

static Error test_atPointer (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 8456, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 7891, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 7894, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo *p = LinkedListFoo_atPointer(llfoo, 2);
	assert(1234 == p->a);
	p = LinkedListFoo_atPointer(llfoo, 1);
	assert(7891 == p->a);
	return ERROR_NONE;
}

static Error test_first (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 8456, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addFirst(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo p = LinkedListFoo_first(llfoo);
	assert(1234 == p.a);
	return ERROR_NONE;
}

static Error test_last (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 8456, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo p = LinkedListFoo_last(llfoo);
	assert(1234 == p.a);
	return ERROR_NONE;
}

static Error test_at (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 8456, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 7891, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 1234, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	e = LinkedListFoo_addLast(llfoo, (Foo){.a = 7894, .b = {1, 2, 3, 4}});
	assert(ERROR_NONE == e);
	Foo p = LinkedListFoo_at(llfoo, 2);
	assert(1234 == p.a);
	p = LinkedListFoo_at(llfoo, 1);
	assert(7891 == p.a);
	return ERROR_NONE;
}

static Error test_clone (Void)
{
	P_LinkedListFoo llfoo CLEANUP(LinkedListFoo_finalizeCleanup);
	Error e = LinkedListFoo_initialize(&llfoo);
	assert(ERROR_NONE == e);
	assert(llfoo);
	for (Index i = 0; i < 15; ++i)
	{
		e = LinkedListFoo_addLast(llfoo,
					  (Foo){.a = i, .b = {1, 2, 3, 4}});
		assert(ERROR_NONE == e);
	}	
	P_LinkedListFoo clone CLEANUP(LinkedListFoo_finalizeCleanup);
	e = LinkedListFoo_clone(&clone, llfoo);
	assert(ERROR_NONE == e);
	assert(clone);
	assert(LinkedListFoo_length(llfoo) == LinkedListFoo_length(clone));
	Iterator iLlfoo = LinkedListFoo_iterator(llfoo);
	Iterator iClone = LinkedListFoo_iterator(clone);
	while(Iterator_hasNext(iLlfoo) && Iterator_hasNext(iClone))
	{
		assert(Iterator_next(iLlfoo).a == Iterator_next(iClone).a);
	}
	return ERROR_NONE;
}

C_Test gLinkedlist_tests[] = {
	{.proc = test_initialize,   .caption = "Initialize"},
	{.proc = test_addFirst,     .caption = "Add first"},
	{.proc = test_addLast,      .caption = "Add last"},
	{.proc = test_add,          .caption = "Add"},
	{.proc = test_removeFirst,  .caption = "Remove first"},
	{.proc = test_removeLast,   .caption = "Remove last"},
	{.proc = test_remove,       .caption = "Remove"},
	{.proc = test_length,       .caption = "Length"},
	{.proc = test_iterator,     .caption = "Iterator"},
	{.proc = test_clear,        .caption = "Clear"},
	{.proc = test_firstPointer, .caption = "First pointer"},
	{.proc = test_lastPointer,  .caption = "Last pointer"},
	{.proc = test_atPointer,    .caption = "At pointer"},
	{.proc = test_first,        .caption = "First"},
	{.proc = test_last,         .caption = "Last"},
	{.proc = test_at,           .caption = "At"},
	{.proc = test_clone,        .caption = "Clone"},
	{.proc = NULL,              .caption = "End mark"}
};


/*
int main (void)
{
	Test_do(gLinkedlist_tests);
}
*/
