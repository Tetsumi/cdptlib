/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <assert.h>
#include "../source/base.h"
#include "../source/linkedlist.h"
#include "benchmark.h"

static Time benchmark_addFirst (U64 n)
{	
	P_LinkedListU64 ll CLEANUP(LinkedListU64_finalizeCleanup);
	LinkedListU64_initialize(&ll);
	C_Time start = Benchmark_time();
	for (Index i = 0; i < n; ++i)
		LinkedListU64_addFirst(ll, i);
	C_Time elapsed = Benchmark_time() - start;
	return elapsed;
}

static Time benchmark_iterator (uint64_t n)
{
	P_LinkedListU64 ll CLEANUP(LinkedListU64_finalizeCleanup);
	LinkedListU64_initialize(&ll);
	for (Index i = 0; i < n; ++i)
		LinkedListU64_addFirst(ll, i);
	U64 a = 0;
	C_Time start = Benchmark_time();	
	Iterator(ll) iter = LinkedListU64_iterator(ll);	
	while(iter)
		a += *Iterator_nextPointer(iter);
	C_Time elapsed = Benchmark_time() - start;
	//printf("%lu\n", a);
	return elapsed;
}

int main (void)
{
	puts("Add first:");
	{
		double sum = 0.0;
		for (int i = 0; i < 10; ++i)
		{
			double t = benchmark_addFirst(10000000)/1000000.0;
			printf("%f ms\n", t);
			sum += t;
		}
		printf("mean: %f ms\n", sum/10.0);
	}
	puts("Iterator:");
	{
		double sum = 0.0;
		for (int i = 0; i < 10; ++i)
		{
			double t = benchmark_iterator(10000000)/1000000.0;
			printf("%f ms\n", t);
			sum += t;
		}
		printf("mean: %f ms\n", sum/10.0);
	}
}
