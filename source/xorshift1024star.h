/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <assert.h>
#include "base.h"

#define NUM_SEEDS 16 // Must be a Power of two

typedef struct
{
        Seed64 seeds[NUM_SEEDS];
	U8     position;
} Xorshift1024Star;

DEFINE_TYPEDEFS(Xorshift1024Star);

static inline Void Xorshift1024Star_initialize
(CP_Xorshift1024Star xs, Seed64 seed)
{
	assert(xs);
	assert(0 != seed);
	xs->position = 0;
	for (Index i = 0; i < NUM_SEEDS; ++i)
	{
	        seed ^= seed >> 12;
		seed ^= seed << 25;
		seed ^= seed >> 27;
		xs->seeds[i] = seed * 2685821657736338717LL;
	}
	
}

static inline U64 Xorshift1024Star_nextU64 (CP_Xorshift1024Star xs)
{
	assert(xs);
	Seed64 *seeds   = xs->seeds;
	U8     position = xs->position;
        Seed64 s0       = seeds[position];
	position        = (position + 1) & (NUM_SEEDS - 1);
	Seed64 s1       = seeds[position];
	s1 ^= s1 << 31;
	s1 ^= s1 >> 11;
	s0 ^= s0 >> 30;
	s1 ^= s0;
	seeds[position] = s1;
	xs->position = position;
	return s1 * 1181783497276652981LL; 
}

static inline R64 Xorshift1024Star_nextR64 (CP_Xorshift1024Star xs)
{
	assert(xs);
	return (Xorshift1024Star_nextU64(xs) >> 11)
		* (1.0 / 9007199254740991.0);
}

static inline R32 Xorshift1024Star_nextR32 (CP_Xorshift1024Star xs)
{
	assert(xs);
	return (Xorshift1024Star_nextU64(xs) >> 40) * (1.0 / 16777216.0);
}

static inline Boolean Xorshift1024Star_nextBoolean (CP_Xorshift1024Star xs)
{
	assert(xs);
	return Xorshift1024Star_nextU64(xs) >> 63;
}

static inline U64 Xorshift1024Star_nextMaxU64
(CP_Xorshift1024Star xs, U64 max)
{
	assert(xs);
	U64 value;
	while (true)
	{
		C_U64 p = Xorshift1024Star_nextU64(xs) >> 1;
		value = p % max;
		if (p - value + (max - 1) >= 0)
			break;
	}
	return value;
}

static inline S64 Xorshift1024Star_nextS64 (CP_Xorshift1024Star xs)
{
	assert(xs);
        return (S64)Xorshift1024Star_nextU64(xs);
}
#undef NUM_SEEDS
