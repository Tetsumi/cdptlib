/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <assert.h>
#include "base.h"

#define NUM_SEEDS 624

typedef struct
{
	Seed32 seeds[NUM_SEEDS];
	U32    position;
} MT19937;

DEFINE_TYPEDEFS(MT19937);

static inline Void MT19937_initialize (CP_MT19937 mt, Seed32 seed)
{
	assert(mt);
	mt->position = NUM_SEEDS;
	for (Index i = 0; i < NUM_SEEDS; ++i)
	{
		mt->seeds[i] = seed;
		seed = 1812433253UL * (seed ^ (seed >> 30)) + (i + 1);
	}
}

static inline U32 MT19937_nextU32 (CP_MT19937 mt)
{
	assert(mt);
	if (NUM_SEEDS <= mt->position)
	{
		for (Index i = 0; i < NUM_SEEDS; ++i)
		{
			C_U32 t = (mt->seeds[i] & 0x80000000UL)
				+ (mt->seeds[(i + 1) % 624] & 0x7fffffffUL);
		        C_U32 u = mt->seeds[(i + 397) % 624] ^ (t >> 1);
			mt->seeds[i] = t & 1 ? u ^ 0x9908b0dfUL : u;
		}
		mt->position = 0;
	}
	U32 r = mt->seeds[mt->position++];
	r ^= (r >> 11);
	r ^= (r <<  7) & 0x9d2c5680UL;
	r ^= (r << 15) & 0xefc60000UL;
	r ^= (r >> 18);
	return r;
}

static inline S32 MT19937_nextS32 (CP_MT19937 mt)
{
	assert(mt);
	return (S32)MT19937_nextU32(mt);
}

static inline R32 MT19937_nextR32 (CP_MT19937 mt)
{
	assert(mt);
	return MT19937_nextU32(mt) * (1.0f / 4294967295.0f);
}

static inline R64 MT19937_nextR64 (CP_MT19937 mt)
{
	assert(mt);
	return ((MT19937_nextU32(mt) >> 5)
		* 67108864.0
		+ (MT19937_nextU32(mt) >> 6))
	       * (1.0 / 9007199254740991.0);
}

static inline Boolean MT19937_nextBoolean (CP_MT19937 mt)
{
	assert(mt);
	return MT19937_nextU32(mt) >> 31;
}

#undef NUM_SEEDS
	
