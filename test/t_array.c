/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include "../source/base.h"
#include "../source/array.h"
#include "test.h"

typedef struct
{
	int a;
	union
	{
		float b;
		char c[4];
	};
	int d[10];
} Foo;

DEFINE_TYPEDEFS(Foo);
DEFINE_ARRAY(Foo);

static Error test_copy (Void)
{
	C_Length length = 125;
	P_ArrayFoo a CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&a, length);
	assert(e == ERROR_NONE);
	assert(a);
	P_ArrayFoo b CLEANUP(ArrayFoo_finalizeCleanup);
	C_Length bLength = 135;
	e = ArrayFoo_initialize(&b, bLength);
	assert(e == ERROR_NONE);
	assert(b);
	assert(ArrayFoo_length(b) == bLength);
	for(Index i = 0; i < length; ++i)
	{
		C_Foo f = {
			.a = i,
			.b = (float)i,
			.d = {1,2,3,4,5,6,7,8,9,10}
		};
		ArrayFoo_set(a, i, f);
	}
	ArrayFoo_copy(b, a);
	for(Index i = 0; i < length; ++i)
	{
		assert(ArrayFoo_get(a, i).a == ArrayFoo_get(b, i).a);
		assert(ArrayFoo_get(a, i).b == ArrayFoo_get(b, i).b);
		for (U8 j = 0; j < 10; ++j)
			assert(ArrayFoo_get(a, i).d[j] == ArrayFoo_get(b, i).d[j]);
	}
	assert(bLength == ArrayFoo_length(b));
	return ERROR_NONE;
}

static Error test_get (Void)
{
	C_Length length = 1;
	P_ArrayFoo f CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&f, length);
	assert(e == ERROR_NONE);
	assert(f);
	f->elements[0].a = 123456;
	assert(123456 == ArrayFoo_get(f, 0).a);
	return ERROR_NONE;
}

static Error test_getPointer (Void)
{
	C_Length length = 1;
	P_ArrayFoo f CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&f, length);
	assert(e == ERROR_NONE);
	assert(f);
	f->elements[0].a = 123456;
	assert(123456 == ArrayFoo_getPointer(f, 0)->a);
	return ERROR_NONE;
}

static Error test_set (Void)
{
	C_Length length = 1;
	P_ArrayFoo f CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&f, length);
	assert(e == ERROR_NONE);
	assert(f);
	ArrayFoo_set(f, 0, (Foo){.a = 123456});
	assert(123456 == ArrayFoo_get(f, 0).a);
	return ERROR_NONE;
}

static Error test_length (Void)
{
	C_Length length = 100;
	P_ArrayFoo f CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&f, length);
	assert(e == ERROR_NONE);
	assert(f);
	assert(ArrayFoo_length(f) == length);
	return ERROR_NONE;
}

static Error test_size (Void)
{
	C_Length length = 100;
	P_ArrayFoo f CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&f, length);
	assert(e == ERROR_NONE);
	assert(f);
	assert((sizeof(Foo) * 100) + sizeof(ArrayFoo) == ArrayFoo_size(f)); 
	return ERROR_NONE;
}

static Error test_clone (Void)
{
	C_Length length = 100;
	P_ArrayFoo f CLEANUP(ArrayFoo_finalizeCleanup);
	Error e = ArrayFoo_initialize(&f, length);
	assert(e == ERROR_NONE);
	assert(f);
	for(Index i = 0; i < length; ++i)
	{
		ArrayFoo_set(f, i, (Foo){.a = i});
	}
	P_ArrayFoo fb CLEANUP(ArrayFoo_finalizeCleanup);
	e = ArrayFoo_clone(&fb, f);
	assert(e == ERROR_NONE);
	assert(fb);
	for(Index i = 0; i < length; ++i)
	{
		assert(ArrayFoo_get(f,i).a == ArrayFoo_get(fb, i).a);
	}
	return ERROR_NONE;
}
			
C_Test gArray_tests[] = {
	{.proc = test_length,     .caption = "Length"},
	{.proc = test_size,       .caption = "Size"},
	{.proc = test_get,        .caption = "Get"},
	{.proc = test_getPointer, .caption = "Get pointer"},
	{.proc = test_set,        .caption = "Set"},
	{.proc = test_copy,       .caption = "Copy"},
	{.proc = test_clone,      .caption = "Clone"},
	{.proc = NULL,            .caption = "End mark"}
};

/*
int main (void)
{
	puts("Testing arrays\n");
	Test_do(gArray_tests);
	puts("Done!");
}
*/

