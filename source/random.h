/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <assert.h>
#include "base.h"
#include "xorshift1024star.h"
#include "mt19937.h"

#define Random_initialize(Context, seed) _Generic(			\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_initialize,	\
		P_MT19937: MT19937_initialize)				\
	((Context), seed)

#define Random_nextU64(Context) _Generic(				\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_nextU64)		\
	((Context))

#define Random_nextU32(Context) _Generic(				\
		(Context),						\
		P_MT19937: MT19937_nextU32)				\
	((Context))

#define Random_nextR64(Context) _Generic(				\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_nextR64,		\
		P_MT19937: MT19937_nextR64)				\
	((Context))

#define Random_nextR32(Context) _Generic(				\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_nextR32,		\
		P_MT19937: MT19937_nextR32)				\
	((Context))

#define Random_nextBoolean(Context) _Generic(				\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_nextBoolean,	\
		P_MT19937: MT19937_nextBoolean)				\
	((Context))

#define Random_nextMaxU64(Context, Max) _Generic(			\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_nextMaxU64)	\
	((Context), Max)

#define Random_nextS64(Context) _Generic(				\
		(Context),						\
		P_Xorshift1024Star: Xorshift1024Star_nextS64)		\
	((Context))

#define Random_nextS32(Context) _Generic(				\
		(Context),						\
		P_MT19937: MT19937_nextS32)		\
	((Context))
