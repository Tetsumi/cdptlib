/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define DEFINE_LINKEDLIST_INITIALIZE(Type)			      \
	static inline Error LinkedList##Type##_initialize	      \
	(CPP_LinkedList##Type ll)				      \
	{							      \
		assert(ll);					      \
		*ll = malloc(sizeof(LinkedList##Type));		      \
		if (!*ll)					      \
			return ERROR_MEMORY;			      \
		**ll = (LinkedList##Type) {			      \
			.length = 0,				      \
			.first  = NULL,				      \
			.last   = NULL,				      \
		};						      \
		return ERROR_NONE;				      \
	}

#define DEFINE_LINKEDLIST_LENGTH(Type) \
	static inline Length LinkedList##Type##_length (CPC_LinkedList##Type ll) \
	{								\
		assert(ll);						\
		return ll->length;					\
	}

#define DEFINE_LINKEDLIST_CLEAR(Type)			\
	static inline Void LinkedList##Type##_clear (P_LinkedList##Type ll) \
	{								\
		assert(ll);						\
		__Node##Type *n = ll->first;				\
		while (n)						\
		{							\
			__Node##Type * const nn = n->next;		\
			free(n);					\
			n = nn;						\
		}							\
		*ll = (LinkedList##Type ){				\
		        .length = 0,					\
			.first = NULL,					\
			.last = NULL					\
		};							\
	}

#define DEFINE_LINKEDLIST_FINALIZE(Type)	\
	static inline Void LinkedList##Type##_finalize (P_LinkedList##Type ll)\
	{								\
		LinkedList##Type##_clear(ll);				\
		free(ll);						\
	}

#define DEFINE_LINKEDLIST_FINALIZE_CLEANUP(Type)	      \
	static inline Void LinkedList##Type##_finalizeCleanup \
	(PP_LinkedList##Type ll)			      \
	{						      \
		assert(ll);				      \
		LinkedList##Type##_finalize(*ll);	      \
	}

#define DEFINE_LINKEDLIST_ITERATOR(Type)			\
	static inline __Node##Type *LinkedList##Type##_iterator	\
	(CPC_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		return ll->first;				\
	}

#define DEFINE_LINKEDLIST_ADD_FIRST(Type)			\
	static inline Error LinkedList##Type##_addFirst		\
	(CP_LinkedList##Type ll, Type element)			\
	{							\
		assert(ll);					\
		__Node##Type *node = malloc(sizeof(*node));	\
		if (!node)					\
			return ERROR_MEMORY;			\
		node->element = element;			\
		node->previous = NULL;				\
		if (ll->first)					\
		{						\
			ll->first->previous = node;		\
			node->next = ll->first;			\
		}						\
		else						\
		{						\
			ll->last = node;			\
			node->next = NULL;			\
		}						\
		ll->first = node;				\
		++(ll->length);					\
		return ERROR_NONE;				\
	}

#define DEFINE_LINKEDLIST_ADD_LAST(Type)			\
	static inline Error LinkedList##Type##_addLast		\
	(CP_LinkedList##Type ll, Type element)			\
	{							\
		assert(ll);					\
		__Node##Type *node = malloc(sizeof(*node));	\
		if (!node)					\
			return ERROR_MEMORY;			\
		node->element = element;			\
		node->next = NULL;				\
		if (ll->last)					\
		{						\
			ll->last->next = node;			\
			node->previous = ll->last;		\
		}						\
		else						\
		{						\
			ll->first = node;			\
			node->previous = NULL;			\
		}						\
		ll->last = node;				\
		++(ll->length);					\
		return ERROR_NONE;				\
	}

#define DEFINE_LINKEDLIST_ADD(Type)					\
	static inline Error LinkedList##Type##_add			\
	(CP_LinkedList##Type ll, C_Index position, Type element)	\
	{								\
		assert(ll);						\
		if (0 == position)					\
			return LinkedList##Type##_addFirst(ll, element); \
		assert(position < LinkedList##Type##_length(ll));	\
		__Node##Type *tNode = ll->first;			\
		for (Index i = 0; i != position; ++i)			\
			tNode = tNode->next;				\
		assert(tNode);						\
		__Node##Type * const node = malloc(sizeof(*node));	\
		if (!node)						\
			return ERROR_MEMORY;				\
		node->element = element;				\
		node->next = tNode;					\
		node->previous = tNode->previous;			\
		tNode->previous->next = node;				\
		tNode->previous = node;					\
		++(ll->length);						\
		return ERROR_NONE;					\
	}

#define DEFINE_LINKEDLIST_FIRST(Type)				\
	static inline Type LinkedList##Type##_first		\
	(CPC_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		assert(ll->first);				\
		return ll->first->element;			\
	}

#define DEFINE_LINKEDLIST_LAST(Type)				\
	static inline Type LinkedList##Type##_last		\
	(CPC_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		assert(ll->last);				\
		return ll->last->element;			\
	}

#define DEFINE_LINKEDLIST_AT(Type)					\
	static inline Type LinkedList##Type##_at			\
	(CPC_LinkedList##Type ll, C_Index position)			\
	{								\
		assert(ll);						\
		assert(position < LinkedList##Type##_length(ll));	\
		__Node##Type *node = ll->first;				\
		for (Index i = 0; i != position; ++i)			\
			node = node->next;				\
		assert(node);						\
		return node->element;					\
	}

#define DEFINE_LINKEDLIST_FIRST_POINTER(Type)			\
	static inline Type *LinkedList##Type##_firstPointer	\
	(CPC_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		assert(ll->first);				\
		return &ll->first->element;			\
	}

#define DEFINE_LINKEDLIST_LAST_POINTER(Type)			\
	static inline Type *LinkedList##Type##_lastPointer	\
	(CPC_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		assert(ll->last);				\
		return &ll->last->element;			\
	}

#define DEFINE_LINKEDLIST_AT_POINTER(Type)				\
	static inline Type *LinkedList##Type##_atPointer		\
	(CPC_LinkedList##Type ll, C_Index position)			\
	{								\
		assert(ll);						\
		assert(position < LinkedList##Type##_length(ll));	\
		__Node##Type *node = ll->first;				\
		for (Index i = 0; i != position; ++i)			\
			node = node->next;				\
		assert(node);						\
		return &node->element;					\
	}

#define DEFINE_LINKEDLIST_REMOVE_FIRST(Type)			\
	static inline Type LinkedList##Type##_removeFirst	\
	(CP_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		assert(ll->first);				\
		Type ret = ll->first->element;			\
		__Node##Type *nFirst = ll->first->next;		\
		free(ll->first);				\
		ll->first = nFirst;				\
		if (nFirst)					\
			nFirst->previous = NULL;		\
		else						\
			ll->last = NULL;			\
		--(ll->length);					\
		return ret;					\
	}

#define DEFINE_LINKEDLIST_REMOVE_LAST(Type)			\
	static inline Type LinkedList##Type##_removeLast	\
	(CP_LinkedList##Type ll)				\
	{							\
		assert(ll);					\
		assert(ll->last);				\
		Type ret = ll->last->element;			\
		__Node##Type *nLast = ll->last->previous;	\
		free(ll->last);					\
		ll->last = nLast;				\
		if (nLast)					\
			nLast->next = NULL;			\
		else						\
			ll->first = NULL;			\
		--(ll->length);					\
		return ret;					\
	}

#define DEFINE_LINKEDLIST_REMOVE(Type)				\
	static inline Type LinkedList##Type##_remove		\
	(CP_LinkedList##Type ll, C_Index position)		\
	{							\
		assert(ll);						\
		if (0 == position)					\
			return LinkedList##Type##_removeFirst(ll); \
		assert(position < LinkedList##Type##_length(ll));	\
		__Node##Type *tNode = ll->first;			\
		for (Index i = 0; i != position; ++i)			\
			tNode = tNode->next;				\
		assert(tNode);						\
		Type ret = tNode->element;				\
		if (tNode->next)					\
		{							\
			tNode->next->previous = tNode->previous;	\
			tNode->previous->next = tNode->next;		\
		}							\
		else							\
		{							\
			tNode->previous->next = NULL;			\
			ll->last = tNode->previous;			\
		}							\
		free(tNode);						\
		--(ll->length);					\
		return ret;					\
	}
#define DEFINE_LINKEDLIST_CLONE(Type)				      \
	static inline Error LinkedList##Type##_clone		      \
	(CPP_LinkedList##Type dest, CPC_LinkedList##Type source)      \
	{							      \
		assert(source);					      \
		assert(dest);					      \
		C_Error e = LinkedList##Type##_initialize(dest);      \
		if (ERROR_NONE != e)				      \
			return e;				      \
		__Node##Type *node;				      \
		__Node##Type *dNode = NULL;			      \
		for (__Node##Type *sNode = source->first;	      \
		     sNode;					      \
		     sNode = sNode->next)			      \
		{						      \
		        node = malloc(sizeof(*node));		      \
			if (!node)				      \
			{					      \
				LinkedList##Type##_finalize(*dest);   \
				return ERROR_MEMORY;		      \
			}					      \
			memcpy(node, sNode, sizeof(*node));	      \
			if (dNode)				      \
				dNode->next = node;		      \
			else					      \
				(*dest)->first = node;		      \
			dNode = node;				      \
			++((*dest)->length);			      \
		}						      \
		(*dest)->last = node;				      \
		return ERROR_NONE;				      \
	}

#define DEFINE_LINKEDLIST(Type)				\
	typedef struct __Node##Type {			\
		struct __Node##Type *next;		\
		struct __Node##Type *previous;		\
		Type element;				\
	} __Node##Type;					\
	typedef struct LinkedList##Type {		\
		Length length;				\
		__Node##Type *last;			\
		__Node##Type *first;			\
	} LinkedList##Type;				\
	DEFINE_TYPEDEFS(LinkedList##Type);		\
	DEFINE_LINKEDLIST_INITIALIZE(Type);		\
	DEFINE_LINKEDLIST_LENGTH(Type);			\
	DEFINE_LINKEDLIST_CLEAR(Type);			\
	DEFINE_LINKEDLIST_FINALIZE(Type);		\
	DEFINE_LINKEDLIST_FINALIZE_CLEANUP(Type);	\
	DEFINE_LINKEDLIST_ITERATOR(Type);		\
	DEFINE_LINKEDLIST_ADD_FIRST(Type);		\
	DEFINE_LINKEDLIST_ADD_LAST(Type);		\
	DEFINE_LINKEDLIST_ADD(Type);			\
	DEFINE_LINKEDLIST_FIRST(Type);			\
	DEFINE_LINKEDLIST_LAST(Type);			\
	DEFINE_LINKEDLIST_AT(Type);			\
	DEFINE_LINKEDLIST_REMOVE_FIRST(Type);		\
	DEFINE_LINKEDLIST_REMOVE_LAST(Type);		\
	DEFINE_LINKEDLIST_REMOVE(Type);			\
	DEFINE_LINKEDLIST_FIRST_POINTER(Type);		\
	DEFINE_LINKEDLIST_LAST_POINTER(Type);		\
	DEFINE_LINKEDLIST_AT_POINTER(Type);		\
	DEFINE_LINKEDLIST_CLONE(Type);

DEFINE_LINKEDLIST(U8);
DEFINE_LINKEDLIST(U16);
DEFINE_LINKEDLIST(U32);
DEFINE_LINKEDLIST(U64);
DEFINE_LINKEDLIST(S8);
DEFINE_LINKEDLIST(S16);
DEFINE_LINKEDLIST(S32);
DEFINE_LINKEDLIST(S64);
DEFINE_LINKEDLIST(R32);
DEFINE_LINKEDLIST(R64);
DEFINE_LINKEDLIST(Size);
DEFINE_LINKEDLIST(Char);
DEFINE_LINKEDLIST(Boolean);
DEFINE_LINKEDLIST(P_Void);
DEFINE_LINKEDLIST(P_U8);
DEFINE_LINKEDLIST(P_U16);
DEFINE_LINKEDLIST(P_U32);
DEFINE_LINKEDLIST(P_U64);
DEFINE_LINKEDLIST(P_S8);
DEFINE_LINKEDLIST(P_S16);
DEFINE_LINKEDLIST(P_S32);
DEFINE_LINKEDLIST(P_S64);
DEFINE_LINKEDLIST(P_R32);
DEFINE_LINKEDLIST(P_R64);
DEFINE_LINKEDLIST(P_Size);
DEFINE_LINKEDLIST(P_Char);
DEFINE_LINKEDLIST(P_Boolean);


#define LinkedList_STATIC {.length = 0, .first = NULL, .last = NULL}

#define Iterator __auto_type

#define Iterator_hasNext(iter) (iter)

#define Iterator_next(iter) (						\
		{							\
			__auto_type e = iter->element;			\
			iter = iter->next;				\
			e;						\
		})

#define Iterator_nextPointer(iter) (					\
		{							\
			__auto_type e = &iter->element;			\
			iter = iter->next;				\
			e;						\
		})
